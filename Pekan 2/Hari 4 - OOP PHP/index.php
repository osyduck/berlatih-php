<?php

require "animal.php";
require "Ape.php";
require "Frog.php";

$sheep = new Animal("shaun");
$sheep->setLegs(4);

echo "Name: " . $sheep->getName() . "\n"; // "shaun"
echo "Legs: " . $sheep->getLegs() . "\n"; // 4
echo "Cold Blooded: " . $sheep->getColdBlooded() . "\n\n"; // "no"

$kodok = new Frog("kuduk");
$kodok->setLegs(4);
echo "Name: " . $kodok->getName() . "\n"; // "kuduk"
echo "Legs: " . $kodok->getLegs() . "\n"; // 2
echo "Cold Blooded: " . $kodok->getColdBlooded() . "\n"; // "no"
$kodok->jump();
echo "\n\n";

$sungokong = new Ape("kera sakti");

echo "Name: " . $sungokong->getName() . "\n"; // "kera sakti"
echo "Legs: " . $sungokong->getLegs() . "\n"; // 4
echo "Cold Blooded: " . $sungokong->getColdBlooded() . "\n"; // "no"
$sungokong->yell();
?>