<?php

class Animal {
    public $name;
    public $legs = 2;
    public $cold_blooded = "no";

    public function __construct($name) {
        $this->name = $name;
    }

    public function getName() {
        return $this->name;
    }

    public function setLegs($legs) {
        $this->legs = $legs;
    }

    public function getLegs() {
        return $this->legs;
    }

    public function getColdBlooded() {
        return $this->cold_blooded;
    }
}


?>