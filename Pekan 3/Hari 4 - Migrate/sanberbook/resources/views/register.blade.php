<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        @csrf
        <label for="fname">First Name:</label></br></br>
        <input type="text" id="firstname" name="firstname"></br></br>

        <label for="lname">Last Name:</label></br></br>
        <input type="text" id="lastname" name="lastname"></br></br>

        <label for="gender">Gender:</label></br></br>
        <input type="radio" id="male" name="gender">
        <label for="male">Male</label></br>
        <input type="radio" id="female" name="gender">
        <label for="female">Female</label></br>
        <input type="radio" id="other" name="gender">
        <label for="other">Other</label></br></br>

        <label for="nationality">Nationality:</label></br></br>
        <select id="nationality_select" name="nationality_select">
            <option value="indonesian">Indonesian</option>
            <option value="malaysian">Malaysian</option>
            <option value="korean">Korean</option>
            <option value="american">American</option>
        </select></br></br>

        <label>Languange Spoken</label><br><br>
		<input type="checkbox" name="languange1">Bahasa Indonesia<br>
		<input type="checkbox" name="languange2">English<br>
		<input type="checkbox" name="languange3">Other<br><br>

        <label for="bio">Bio:</label></br></br>
        <textarea cols="30" rows="10" id="input_bio" name="input_bio"></textarea></br>

        <input type="submit" name="submit" value="Sign Up">
    </form>
</body>

</html>