<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register()
    {
        //
        return view('register');
    }

    public function welcome_post(Request $request)
    {
        $fname = $request->firstname;
        $lname = $request->lastname;
        return view('welcome', ['fname' => $fname, 'lname' => $lname]);
    }
}
