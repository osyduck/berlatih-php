<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    //
    public function index() {
        $casts = DB::table('cast')->get();
        return view('cast.index',compact('casts'));
    }

    public function create() {
        return view('cast.create');
    }

    public function store(Request $request) {
        $casts = DB::table('cast')
        ->insert([
            "nama" => $request['nama'],
            "umur" => $request['umur'],
            "bio" => $request['bio']
        ]);
        return redirect('/cast');
    }

    public function show($id) {
        $casts = DB::table('cast')->where('id', $id)->first();
        //dd($cast);
        return view('cast.show',compact('casts'));
    }

    public function edit($id) {
        $casts = DB::table('cast')->where('id', $id)->first();
        //dd($cast);
        return view('cast.edit',compact('casts'));

    }

    public function update($id, Request $request) {
        $update = DB::table('cast')
        ->where('id',$id)
        ->update([
            "nama" => $request['nama'],
            "umur" => $request['umur'],
            "bio" => $request['bio']
        ]);
        
        return redirect('/cast');
    }

    public function destroy($id) {
        $destroy = DB::table('cast')
        ->where('id',$id)
        ->delete();
        return redirect('/cast');
    }
}
