@extends("master")

@section("content")

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>DataTable Cast</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Contact</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">


  <!-- /.content -->
<div class="card">
    <div class="card-header">
      <h3 class="card-title">DataTable Cast</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <div class="row">
            <div class="col-md-4 mb-2">
                <a class="btn btn-primary" href="{{ route('/cast/create') }}">Tambah Data</a>
            </div>
        </div>
      <table id="example1" class="table table-bordered table-striped">
        <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Nama</th>
          <th scope="col">Umur</th>
          <th scope="col">Bio</th>
          <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @forelse ($casts as $key => $value)

            <tr>
                <td>{{ $key + 1 }}</td>
                <td>{{ $value->nama }}</td>
                <td>{{ $value->umur }}</td>
                <td>{{ $value->bio }}</td>
                <td>
                    <div class="container">
                    <div class="d-flex justify-content-start">
                    <a href="/cast/{{ $value->id}}" class="btn btn-info ml-2">Show</a>
                    <a href="/cast/{{ $value->id}}/edit" class="btn btn-primary ml-2">Edit</a>
                    <form action="/cast/{{ $value->id}}" method="post">
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-danger ml-2" value="Delete">
                    </form>
                        </div>
                    </div>
                    
                </td>
            </tr>
            @empty
            <tr colspan="3">
                <td col>No Data</td>
            </tr>
            @endforelse
        </tbody>
        <tfoot>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nama</th>
                <th scope="col">Umur</th>
                <th scope="col">Bio</th>
                <th scope="col">Actions</th>
              </tr>
        </tfoot>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
</section>
  @endsection
  @push('scripts')
    <script src="../../plugins/datatables/jquery.dataTables.js"></script>
    <script src="../../plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
  @endpush
  