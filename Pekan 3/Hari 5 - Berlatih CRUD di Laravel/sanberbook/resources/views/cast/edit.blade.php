@extends("master")

@section("content")

<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>DataTable</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Contact</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">


  <!-- /.content -->
<div class="card">
    <div class="card-header">
      <h3 class="card-title">DataTable with default features</h3>
    </div>
    <!-- /.card-header -->
    <form action="/cast/{{ $casts->id}}" method="POST">
    <div class="card-body">
        
        <div class="form-group">
            <input type="hidden" value="{{ $casts->id }}">
          <label for="exampleInputEmail1">Nama</label>
          <input type="text" class="form-control" id="exampleInputEmail1"
          value="{{ old('nama',$casts->nama) }}"
          placeholder="Nama" name="nama">
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">Umur</label>
          <input type="number" value="{{ old('umur',$casts->umur) }}"
          min="0" class="form-control" id="exampleInputPassword1" name="umur" placeholder="Umur">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Bio</label>
            <textarea class="form-control" name="bio">{{ $casts->bio }}</textarea>
          </div>
      </div>

    <!-- /.card-body -->
    <div class="card-footer">
        
            @csrf
            @method('PUT')
            <input type="submit" class="btn btn-primary" value="Edit">
            
    </div>
</form>
  </div>
</section>
  @endsection
  