@extends('master')
@section('content')

<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Detail User</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">List Cast</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>


<section class="content">
    <div class="container">
        <div class="card d-flex justify-content-center">
            <div class="card-header">
              <h3 class="card-title">Detail User {{$casts->nama}}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <div class="card-body">
              <h5 class="card-title">Nama: {{ $casts->nama }}</h5>
              <p class="card-text">Umur: {{ $casts->umur}}</p>
              <p class="card-text">Bio: {{ $casts->bio }}</p>
            </div>
              <!-- /.card-body -->
      
              
          </div>
    </div>
  
</section>

@endsection